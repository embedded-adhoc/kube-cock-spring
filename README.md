# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Install helm ###

* Homebrew

		brew install helm

* Chocolatey (Windows)

		choco install kubernetes-helm

* Snap (Linux)
	
		sudo snap install helm --classic

### Add repo stable   ###
	
		helm repo add stable https://kubernetes-charts.storage.googleapis.com
		
		helm repo update

### Install cockcroachdb package###

		helm install my-release stable/cockroachdb

### Acesss with shell to check db ###

	    kubectl run -it --rm cockroach-client \
        --image=cockroachdb/cockroach \
        --restart=Never \
        --command -- \
        ./cockroach sql --insecure --host=my-release-cockroachdb-public.default
        
### Create db to test ###
		
		CREATE DATABASE customers;
     	
     	CREATE User myuser;
		
		CREATE TABLE product(
   			id VARCHAR (50) PRIMARY KEY,
   			description VARCHAR (500),
   			price numeric (10,2) NOT NULL
		);


### exit shell and expose port from minikube ### 
	
		kubectl port-forward my-release-cockroachdb-0 26257
		
### testing with spring project and postman.

		[POST]	http://localhost:8080/product/create
		
		JSON-raw:
		
		{
			"description": "Tut#7 Description",
			"price": 52.0
		}
		
		[GET]	http://localhost:8080/product/all
		
### References ###
*	https://www.cockroachlabs.com/docs/stable/connection-parameters.html
*	http://www.vinsguru.com/spring-boot-integrating-with-distributedsql-cockroachdb/
	

